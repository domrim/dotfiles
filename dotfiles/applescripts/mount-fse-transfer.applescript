-- SSH parameters 
set args to {sshRemote:"fse"}

-- SSHFS parameters
set args to args & {remotePath:"/home/transfer"}
set args to args & {localPath:"~/mount/fse-transfer"}
set args to args & {volumeName:"fse-transfer"}

set scriptPath to POSIX file "/Users/dominik/Library/Scripts/mount-sshfs.scpt"
set sshfsHelpers to load script scriptPath
tell sshfsHelpers to mountSSHFS(args)