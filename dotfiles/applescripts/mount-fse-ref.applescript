-- SSH parameters 
set args to {sshRemote:"fse"}

-- SSHFS parameters
set args to args & {remotePath:"/home/ref"}
set args to args & {localPath:"~/mount/fse-ref"}
set args to args & {volumeName:"fse-ref"}

set scriptPath to POSIX file "/Users/dominik/Library/Scripts/mount-sshfs.scpt"
set sshfsHelpers to load script scriptPath
tell sshfsHelpers to mountSSHFS(args)