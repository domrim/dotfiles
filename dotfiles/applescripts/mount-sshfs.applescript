-- SSH parameters 
set parameters to {sshConfigFile:text returned of (display dialog "SSH-Config File path" default answer "/Users/dominik/.ssh/config")}

set parameters to parameters & {sshKeyFingerprint:text returned of (display dialog "SSH-Key Fingerpring" default answer "wgruihOv/ks30aWUNBnGsMdwCEl0R9AC4obkxEaBjZI")}
checkSSHAgent(sshKeyFingerprint of parameters)

set parameters to parameters & {sshRemote:text returned of (display dialog "Remote SSH-Server (ssh-config-alias or username@server)" default answer "")}
set parameters to parameters & {sshPort:text returned of (display dialog "SSH-Port" default answer "22")}

-- SSHFS parameters
set parameters to parameters & {remotePath:text returned of (display dialog "Remote path on server to mount" default answer "")}
set parameters to parameters & {localPath:text returned of (display dialog "Local mount path" default answer "")}
set parameters to parameters & {volumeName:text returned of (display dialog "Volume name" default answer "")}

mountSSHFS(parameters)

-- Function: checks if SSH-Key is loaded
on checkSSHAgent(keyFingerprint)
	try
		set sshStatus to do shell script "ssh-add -l | grep -q " & keyFingerprint
	on error
		set sshContinue to display dialog "SSH Key locked. Continue?" with icon caution
		log "SSH-Key locked"
	end try
end checkSSHAgent

-- Function: Mount remote dir with given arguments, all not given arguments will be set to default values:
-- Default Values:
--   sshConfigFile: "/Users/dominik/.ssh/config"
--   sshPort:"22"
--   sshKeyFingerprint:"wgruihOv/ks30aWUNBnGsMdwCEl0R9AC4obkxEaBjZI"
on mountSSHFS(args)
	set defaults to {sshConfigFile:"/Users/dominik/.ssh/config", sshPort:"22", sshKeyFingerprint:"wgruihOv/ks30aWUNBnGsMdwCEl0R9AC4obkxEaBjZI"}
	set args to args & defaults

	-- Check for unlocked ssh-key
	checkSSHAgent(sshKeyFingerprint of args)

	-- Try to unmount
	UnmountVolume(localPath of args)
	
	-- Check local directory path
	CheckVolumeDirectory(localPath of args)
	
	-- Try to mount
	MountVolume(args)
end mountSSHFS

-- Function: Create directory if not exists
on CheckVolumeDirectory(aPath)
	tell application "System Events"
		if exists folder aPath then
		else
			try
				do shell script "mkdir " & aPath
			end try
		end if
	end tell
end CheckVolumeDirectory

-- Function: Unmout volume at path
on UnmountVolume(aPath)
	try
		do shell script "/usr/sbin/diskutil umount force " & aPath
		log "Unmount SSHFS Volume: " & aPath
	end try
end UnmountVolume

-- Function: Mount volume at path
on MountVolume(args)
	try
		do shell script "{{@@ brew_prefix @@}}/bin/sshfs -F " & sshConfigFile of args & " -p " & sshPort of args & " " & sshRemote of args & ":" & remotePath of args & " " & localPath of args & " -o local,reconnect,defer_permissions,allow_other,noappledouble,auto_cache,volname=" & volumeName of args
		log "Mount SSHFS Volume: " & localPath
	end try
end MountVolume
