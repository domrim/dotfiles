## Server management utilities
function moderate_mails {
ssh -t fse "sudo /usr/local/sbin/list_requests -H"
}

### Set BG Monitor brightness command
function bg-brightness() {
m1ddc display 1 set luminance ${1}
m1ddc display 3 set luminance ${1}
}
