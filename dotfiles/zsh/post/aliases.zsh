# Common aliases
alias vi="nvim"
alias vim="nvim"

# App aliases
{%@@ if meld and platform == "darwin" @@%}
alias meld="open -W -a Meld $@"

{%@@ endif @@%}
# Host dependent content
# OS dependent content
{%@@ if platform == "darwin" @@%}
export HOMEBREW_NO_ANALYTICS=1
alias unlock_app="sudo xattr -rd com.apple.quarantine"
alias mtr="sudo mtr"
alias wget="curl -L -O --retry 999 --retry-max-time 0 -C -"
alias rgrep="grep -r"
alias pinentry="{{@@ brew_prefix @@}}/bin/pinentry-mac"
alias cssh="~/Library/Application\ Support/iTerm2/iterm2env/versions/3.10.4/bin/python ~/code/private/iterm2-clusterssh/i2cssh.py"
{%@@ endif @@%}

{%@@ if bat @@%}
alias cat="bat"
{%@@ endif @@%}
{%@@ if eza @@%}
alias ls="eza"
{%@@ endif @@%}
{%@@ if fzf and bat @@%}
alias pf="fzf --preview='bat --color=always --style=numbers {}' --bind=shift-up:preview-half-page-up,shift-down:preview-half-page-down"
{%@@ endif @@%}
{%@@ if gibo @@%}
alias gi="gibo"
{%@@ endif @@%}

## DEV Setups
{%@@ if custom_go @@%}
export PATH="/usr/local/go/bin:$PATH"
{%@@ endif @@%}
