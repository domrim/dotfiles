## Color echoing taken from tldp.org/LDP/abs/html/colorizing.html
# Defining colors for output
black='\033[30m'
red='\033[31m'
green='\033[32m'
yellow='\033[33m'
blue='\033[34m'
magenta='\033[35m'
cyan='\033[36m'
white='\033[37m'
reset='\033[0m'

# color-echo function
cecho () {
  local default_msg="No message passed."  # Doesn't really need to be a local variable.
  message=${1:-$default_msg}  # Defaults to default message.
  color=${2:-$black}  # Defaults to black, if not specified.

  printf "$color"
  printf "$message"
  printf "$reset\n"
  return
}

bold=$(tput bold)
normal=$(tput sgr0)

