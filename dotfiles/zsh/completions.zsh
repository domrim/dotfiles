znap function __pip pip 'eval "$( pip completion --zsh )"'
compctl -K __pip pip

znap fpath _dotdrop 'curl -fsSL https://raw.githubusercontent.com/deadc0de6/dotdrop/master/completion/_dotdrop-completion.zsh'

{%@@ if platform != "darwin" @@%}
znap function _python_argcomplete pipx  'eval "$(register-python-argcomplete pipx)"'
complete -o nospace -o default -o bashdefault -F _python_argcomplete pipx

{%@@ endif @@%}
{%@@ if poetry and platform != "darwin" @@%}
znap fpath _poetry_{{@@ poetry_path_hash @@}}_complete 'poetry completions zsh'

{%@@ endif @@%}
{%@@ if mbc @@%}
znap function _mbc_completion mbc 'eval "$(_MBC_COMPLETE=zsh_source mbc)"'
compdef _mbc_completion mbc

{%@@ endif @@%}
{%@@ if synadm @@%}
znap function _synadm_completion synadm 'eval "$(_SYNADM_COMPLETE=zsh_source synadm)"'
compdef _synadm_completion synadm

{%@@ endif @@%}
{%@@ if pve_cli @@%}
znap function _pve_cli_completion pve-cli 'eval "$(pve-cli --show-completion)"'
compdef _pve_cli_completion pve-cli

{%@@ endif @@%}
{%@@ if ecsmgmt @@%}
znap function _ecsmgmt_completion ecsmgmt '_ECSMGMT_COMPLETE=zsh_source ecsmgmt'
compdef _ecsmgmt_completion ecsmgmt

{%@@ endif @@%}
{%@@ if rustup @@%}
znap fpath _rustup 'rustup completions zsh'
znap fpath _cargo  'rustup completions zsh cargo'
{%@@ endif @@%}
