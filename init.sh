#! /usr/bin/env bash
# shellcheck disable=SC2059,SC2034

set -e

## Color echoing taken from tldp.org/LDP/abs/html/colorizing.html
# Defining colors for output
black='\033[30m'
red='\033[31m'
green='\033[32m'
yellow='\033[33m'
blue='\033[34m'
magenta='\033[35m'
cyan='\033[36m'
white='\033[37m'
reset='\033[0m'

# color-echo function
cecho () {
  local default_msg="No message passed."  # Doesn't really need to be a local variable.
  message=${1:-$default_msg}  # Defaults to default message.
  color=${2:-$black}  # Defaults to black, if not specified.

  printf "$color"
  printf "$message"
  printf "$reset\n"
  return
}

cecho "Installing python3 with pipx, curl, nvim, chsh & zsh" "$cyan"
printf "$blue"
if command -v dnf > /dev/null; then
  sudo dnf install python3 curl zsh neovim util-linux-user pipx
elif command -v apt-get > /dev/null; then
  sudo apt-get install python3 curl zsh neovim pipx python3-venv
elif command -v brew > /dev/null && ! brew bundle check --file=brewfile-init; then
  brew bundle install --file=brewfile-init --no-lock
fi
printf "$reset"

if ! [ "$SHELL" = "$(which zsh)" ]; then
    cecho "Set zsh as default shell" "$cyan"
    chsh -s "$(which zsh)"
fi

if ! command -v dotdrop >/dev/null 2>&1; then
    cecho "Installing dotdrop" "$cyan"
    printf "$blue"
    pipx install dotdrop
fi

cecho "Done :)" "$green"
printf "$reset"

