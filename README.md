# dotfiles

Dotfile repo. Dotfiles are managed with [dotdrop by deadc0de6](https://github.com/deadc0de6/dotdrop)

## Install
1. `./init.sh`
2. `dotdrop install`

## macOS Bootstrap

* iTerm2 Theme: https://github.com/kepano/flexoki
* set some defaults
  * `defaults write -g InitialKeyRepeat -int 15`
* disable sshd
  * `launchctl disable gui/501/com.openssh.ssh-agent`
  * `launchctl stop com.openssh.ssh-agent`
  * check with `ps aux | grep ssh-agent`
    * kill old `/usr/bin/ssh-agent -l` process

### Caveats

#### why we don't use zshenv

TL;DR: `path_helper` sorts the PATH between `.zshenv` and `.zprofile`

https://gist.github.com/Linerre/f11ad4a6a934dcf01ee8415c9457e7b2

#### poetry install fails with certificate error
symptome:
```
curl -sSL https://install.python-poetry.org | python3 -
```
fails with
```
ssl.SSLError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:749)
```

solution (https://github.com/Homebrew/homebrew-core/issues/42198#issuecomment-564181886):
```
brew reinstall openssl
```
